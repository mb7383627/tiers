In scripts.txt or module_scripts.py 


This script is for when the player goes to recruit from a village, for NPC lords, it's "update_npc_volunteer_troops_in_village"

```
("update_volunteer_troops_in_village",
    [
    
       (store_script_param, ":center_no", 1),
       (party_get_slot, ":player_relation", ":center_no", slot_center_player_relation),
       (party_get_slot, ":center_culture", ":center_no", slot_center_culture),
       
       (faction_get_slot, ":volunteer_troop", ":center_culture", slot_faction_tier_1_troop),
       (assign, ":volunteer_troop_tier", 1),
```

For every 10 relations, there's a chance at getting a higher tier
    
``
       (store_div, ":tier_upgrades", ":player_relation", 10),
``

.

Loops through each tier, giving a 10% chance at upgrading to the next tier
```
       
       (try_for_range, ":unused", 0, ":tier_upgrades"),
         (store_random_in_range, ":random_no", 0, 100),
```

.

There's a 10% chance in each loop. To get tier 2 it's a 10% chance. To get tier 3 it's a 1% chance, .1% for tier 4 etc.

But the higher your relations, the more rolls you get since it loops for "relations / 10" number of times

``
         (lt, ":random_no", 10),
``

.

50/50 chance at getting upgrade path 1 or 2

```
         (store_random_in_range, ":random_no", 0, 2),
         (troop_get_upgrade_troop, ":upgrade_troop_no", ":volunteer_troop", ":random_no"),
         (try_begin),
           (le, ":upgrade_troop_no", 0),
           (troop_get_upgrade_troop, ":upgrade_troop_no", ":volunteer_troop", 0),
         (try_end),
         
         (gt, ":upgrade_troop_no", 0),
         (val_add, ":volunteer_troop_tier", 1),
         (assign, ":volunteer_troop", ":upgrade_troop_no"),
       (try_end),
```

.


Figures out how many troops you can recruit from the village
```
       (assign, ":upper_limit", 8),
       (try_begin),
         (ge, ":player_relation", 4),
         (assign, ":upper_limit", ":player_relation"),
         (val_div, ":upper_limit", 2),
         (val_add, ":upper_limit", 6),
       (else_try),
         (lt, ":player_relation", 0),
         (assign, ":upper_limit", 0),
       (try_end),

       (val_mul, ":upper_limit", 3),   
       (store_add, ":amount_random_divider", 2, ":volunteer_troop_tier"),
       (val_div, ":upper_limit", ":amount_random_divider"),
       
       (store_random_in_range, ":amount", 0, ":upper_limit"),
       (party_set_slot, ":center_no", slot_center_volunteer_troop_type, ":volunteer_troop"),
       (party_set_slot, ":center_no", slot_center_volunteer_troop_amount, ":amount"),
     ]),
```

.

```
update_volunteer_troops_in_village -1
    35 - NUMBER OF STATEMENTS IN THIS SCRIPT
    23 2 1224979098644774912 1 
    521 3 1224979098644774913 1224979098644774912 26 
    521 3 1224979098644774914 1224979098644774912 19 
    522 3 1224979098644774915 1224979098644774914 41 
    2133 2 1224979098644774916 1 
    2123 3 1224979098644774917 1224979098644774913 10 
    6 3 1224979098644774918 0 1224979098644774917 
       2136 3 1224979098644774919 0 100 
       2147483678 2 1224979098644774919 10 
       2136 3 1224979098644774919 0 2 
       1561 3 1224979098644774920 1224979098644774915 1224979098644774919 
       4 0 
           2147483680 2 1224979098644774920 0 
           1561 3 1224979098644774920 1224979098644774915 0 
       3 0 
       32 2 1224979098644774920 0 
       2105 2 1224979098644774916 1 
       2133 2 1224979098644774915 1224979098644774920 
    3 0 
    2133 2 1224979098644774921 8 
    4 0 
       30 2 1224979098644774913 4 
       2133 2 1224979098644774921 1224979098644774913 
       2108 2 1224979098644774921 2 
       2105 2 1224979098644774921 6 
    5 0 
       2147483678 2 1224979098644774913 0 
       2133 2 1224979098644774921 0 
    3 0 
    2107 2 1224979098644774921 3 
    2120 3 1224979098644774922 2 1224979098644774916 
    2108 2 1224979098644774921 1224979098644774922 
    2136 3 1224979098644774923 0 1224979098644774921 
    501 3 1224979098644774912 92 1224979098644774915 
    501 3 1224979098644774912 93 1224979098644774923 

```



```
("update_volunteer_troops_in_village",
    [
       (store_script_param, ":center_no", 1),
       (party_get_slot, ":player_relation", ":center_no", slot_center_player_relation),
       (party_get_slot, ":center_culture", ":center_no", slot_center_culture),
	  	   
       (faction_get_slot, ":volunteer_troop", ":center_culture", slot_faction_tier_1_troop),
       (assign, ":volunteer_troop_tier", 1),
       (store_div, ":tier_upgrades", ":player_relation", 10),
       (try_for_range, ":unused", 0, ":tier_upgrades"),
         (store_random_in_range, ":random_no", 0, 100),
         (lt, ":random_no", 10),
         (store_random_in_range, ":random_no", 0, 2),
         (troop_get_upgrade_troop, ":upgrade_troop_no", ":volunteer_troop", ":random_no"),
         (try_begin),
           (le, ":upgrade_troop_no", 0),
           (troop_get_upgrade_troop, ":upgrade_troop_no", ":volunteer_troop", 0),
         (try_end),
         (gt, ":upgrade_troop_no", 0),
         (val_add, ":volunteer_troop_tier", 1),
         (assign, ":volunteer_troop", ":upgrade_troop_no"),
       (try_end),
       
       (assign, ":upper_limit", 8),
       (try_begin),
         (ge, ":player_relation", 4),
         (assign, ":upper_limit", ":player_relation"),
         (val_div, ":upper_limit", 2),
         (val_add, ":upper_limit", 6),
       (else_try),
         (lt, ":player_relation", 0),
         (assign, ":upper_limit", 0),
       (try_end),

       (val_mul, ":upper_limit", 3),   
       (store_add, ":amount_random_divider", 2, ":volunteer_troop_tier"),
       (val_div, ":upper_limit", ":amount_random_divider"),
       
       (store_random_in_range, ":amount", 0, ":upper_limit"),
       (party_set_slot, ":center_no", slot_center_volunteer_troop_type, ":volunteer_troop"),
       (party_set_slot, ":center_no", slot_center_volunteer_troop_amount, ":amount"),
     ]),
```

.

If you're playing a mod, the numbers will most likely be different except for the operations. So ``2147483678 2`` and ``2123 3`` would probably be the same.

.

If we want to get the highest tier that our relations allows, every time. We can change the 10% to a 100% chance.

``
2147483678 2 1224979098644774919 10 
``

``
         (lt, ":random_no", 10),
``

.

If we wanted to change the required relations for the chance at a higher tier, we can set the 10 to a 1. For every 1 relation we get a shot at a higher tier, if there's only 6 tiers and we have 10 relations, we get an extra 4 rolls in the 10% chance to upgrade loop. This might make the game a bit slower although it won't be anything major.

``
	2123 3 1224979098644774917 1224979098644774913 10 
``

``
       (store_div, ":tier_upgrades", ":player_relation", 10),
``

